document.addEventListener('DOMContentLoaded', function () {
    const videoElement = document.createElement('video');
	const videoContainer = document.getElementById("video-container");
    videoContainer.appendChild(videoElement);

	videoElement.style.width = videoContainer.dataset.width;
	videoElement.style.height = videoContainer.dataset.height;
    videoElement.controls = true;

    const videoUrl = 'https://raw.githubusercontent.com/Hiwo-1101000-1101001/video_for_test/main/file_example_MP4_480_1_5MG.mp4';

    // Устанавливаем источник видео
    videoElement.src = videoUrl;

    // Воспроизводим видео
    videoElement.play().catch(function(error) {
        console.error('Error auto-playing the video:', error);
    });
});
